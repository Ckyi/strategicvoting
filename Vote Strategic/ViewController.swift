//
//  ViewController.swift
//  Vote Strategic
//
//  Created by orange on 2015-08-23.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var coreLocation : CoreLocationController = CoreLocationController()
    
    @IBOutlet var gpsButton: UIButton!
    @IBOutlet var postcodeButton: UIButton!
    
    @IBAction func gpsButtonPressed(sender: UIButton) {
        coreLocation.getAutorisation();
    }
    
    
    @IBAction func postalCodeButtonPressed(sender: UIButton) {
        let postcodeViewControllerObejct = self.storyboard?.instantiateViewControllerWithIdentifier("PostCodeViewControllerIdentifier") as? PostCodeViewController
        self.navigationController?.pushViewController(postcodeViewControllerObejct!, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.gpsButton.layer.cornerRadius = 4;
        self.gpsButton.layer.borderWidth = 1;
        self.gpsButton.layer.borderColor = UIColor.blackColor().CGColor
        self.postcodeButton.layer.cornerRadius = 4;
        self.postcodeButton.layer.borderWidth = 1;
        self.postcodeButton.layer.borderColor = UIColor.blackColor().CGColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "ShowGpsView"
        {
            if let destinationVC = segue.destinationViewController as? GPSViewController{
                //destinationVC.myRidingText = "Ottawa-Nepean"
                coreLocation.delegate = destinationVC
            }
            
        }
    }
    
}

