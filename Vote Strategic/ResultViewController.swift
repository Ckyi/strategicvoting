//
//  ResultViewController.swift
//  Vote Strategic
//
//  Created by orange on 2015-09-13.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    @IBOutlet var partyCollectionView: UICollectionView!
    @IBOutlet var voteCountCollectionView: UICollectionView!
    
    @IBOutlet var riding: UILabel!
    var ridingData: String?
    var electionData : Array<Dictionary<String, String>>?
    var province: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
        partyCollectionView!.registerClass(PartyCell.self, forCellWithReuseIdentifier: "PartyCell")
        voteCountCollectionView!.registerClass(VoteCountCell.self, forCellWithReuseIdentifier: "VoteCountCell")
        
        // Initialize data
        self.riding.text = ridingData
        self.electionData = Array<Dictionary<String, String>>()
        self.loadCSV("41")
    }
    
    /*func getFileName(resourcePath: String, filePrefix: String)->String
    {
        let docsPath = NSBundle.mainBundle().resourcePath! + resourcePath
        let fileManager = NSFileManager.defaultManager()
        let docsArray: Array<String>
        
        do {
            docsArray = try fileManager.contentsOfDirectoryAtPath(docsPath)
        }
        catch {
            print(error)
        }
        
   
        
    }*/

    func loadCSV(Election: String) {
        //Load CSV data
        //let path = NSBundle.mainBundle().pathForResource(ridingData!+"*", ofType: "csv", inDirectory: //String(Election) + "/" + province! + "/summary")
        
        if ((Election == "41") || (Election == "40") || (Election == "39")) {
           // let filename = getFileName(Election + "/" + province! + "/summary", filePrefix: ridingData!)
            
            CSVScanner.runFunctionOnRowsFromFile(["Electoral District Number","Sir Name","First Name","Party","Vote Count"], withFileName: ridingData!, directory: Election + "/" + province! + "/summary",withFunction: {
            
                (aRow:Dictionary<String, String>) in
                electionData?.append(aRow)
            })
        }
        
        print(self.electionData?[1])

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.electionData!.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        if collectionView == self.partyCollectionView {
            let partyCell = collectionView.dequeueReusableCellWithReuseIdentifier("PartyCell", forIndexPath: indexPath) as! PartyCell
            
            partyCell.data.text = self.electionData?[indexPath.section]["Party"]
            print(indexPath.row)
            
            return partyCell
        }
        else{
            let voteCountCell = collectionView.dequeueReusableCellWithReuseIdentifier("VoteCountCell", forIndexPath: indexPath) as! VoteCountCell

            voteCountCell.data.text = self.electionData?[indexPath.section]["Vote Count"]
            
            
            
            return voteCountCell
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    

}


