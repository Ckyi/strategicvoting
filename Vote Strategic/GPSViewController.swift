//
//  GPSViewController.swift
//  Vote Strategic
//
//  Created by orange on 2015-08-23.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit
import CoreLocation

class GPSViewController: UIViewController, CoreLocationControllerDelegate {
 
    @IBOutlet var myRiding: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myRiding.text = "Finding your location...." //myRidingText
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateLocation(placemark: CLPlacemark) {
        //myRiding.text = String(format: "%s\r\n%s\r\n%s",placemark.locality,placemark.postalCode,
          //  placemark.administrativeArea)
        myRiding.text = placemark.locality! + "\n" + placemark.postalCode! + "\n" + placemark.administrativeArea!
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
