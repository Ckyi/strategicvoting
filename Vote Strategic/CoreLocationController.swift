//
//  CoreLocationController.swift
//  Vote Strategic
//
//  Created by orange on 2015-08-25.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit
import CoreLocation

class CoreLocationController: NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var delegate : CoreLocationControllerDelegate?
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private var authorised : Bool = false {
        didSet {
            if authorised == true {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func getAutorisation()
    {
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        } else if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            authorised = true
        } else if CLLocationManager.authorizationStatus() == .Denied {
            // catch this
        }
    }
        
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .AuthorizedWhenInUse {
            authorised = true
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription, terminator: "")
                return
            }
            
            if placemarks!.count > 0 {
                /*let pm = placemarks[0] as! CLPlacemark
                self.displayLocationInfo(pm)
                */
                if let pm = placemarks?.first {
                    self.displayLocationInfo(pm)
                }
            } else {
                print("Problem with the data received from geocoder", terminator: "")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
       // if placemark != nil {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            print(placemark.locality, terminator: "")
            print(placemark.postalCode, terminator: "")
            print(placemark.administrativeArea, terminator: "")
            print(placemark.country, terminator: "")
            self.delegate?.updateLocation(placemark)
        //}
    }
    
}

protocol CoreLocationControllerDelegate {
    
    func updateLocation(placemark: CLPlacemark)
    
}



