//
//  CSVScanner.swift
//  Vote Strategic
//
//  Created by orange on 2015-09-17.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import Foundation

class CSVScanner {
    
    class func debug(string:String){
        
        print("CSVScanner: \(string)")
    }
    
    class func runFunctionOnRowsFromFile(theColumnNames:Array<String>, withFileName theFileName:String, directory: String, withFunction theFunction:(Dictionary<String, String>)->()) {
        
        if let strBundle = NSBundle.mainBundle().pathForResource(theFileName, ofType: "csv", inDirectory: directory) {

            var encodingError:NSError? = nil
            
            do {
                let fileObject = try NSString(contentsOfFile: strBundle, encoding: NSUTF8StringEncoding)
                
                var fileObjectCleaned = fileObject.stringByReplacingOccurrencesOfString("\r", withString: "\n")
                
                fileObjectCleaned = fileObjectCleaned.stringByReplacingOccurrencesOfString("\n\n", withString: "\n")
                
                let objectArray = fileObjectCleaned.componentsSeparatedByString("\n")
                
                for anObjectRow in objectArray {
                    
                    let objectColumns = anObjectRow.componentsSeparatedByString(",")
                    
                    var aDictionaryEntry = Dictionary<String, String>()
                    
                    var columnIndex = 0
                    
                    for anObjectColumn in objectColumns {
                        
                        aDictionaryEntry[theColumnNames[columnIndex]] = anObjectColumn.stringByReplacingOccurrencesOfString("\"", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil)
                        
                        columnIndex++
                    }
                    
                    if aDictionaryEntry.count>1{
                        theFunction(aDictionaryEntry)
                    }else{
                        
                        CSVScanner.debug("No data extracted from row: \(anObjectRow) -> \(objectColumns)")
                    }
                }
            } catch let error as NSError {
                encodingError = error
                CSVScanner.debug("Unable to load csv file from path: \(strBundle)")
                
                if let errorString = encodingError?.description {
                    
                    CSVScanner.debug("Received encoding error: \(errorString)")
                }
            }
        }else{
            CSVScanner.debug("Unable to get path to csv file: \(theFileName).csv")
        }
    }
}
