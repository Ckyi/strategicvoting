//
//  VoteCountCell.swift
//  Vote Strategic
//
//  Created by orange on 2015-09-25.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit

class VoteCountCell: UICollectionViewCell {
    var data: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
                
        data = UILabel(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        data.font = UIFont.systemFontOfSize(UIFont.smallSystemFontSize())
        data.textColor = UIColor.blackColor()
        data.textAlignment = .Center
        contentView.addSubview(data)
    }
    
}
