//
//  Rest.swift
//  Vote Strategic
//
//  Created by orange on 2015-09-07.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit

protocol RestDelegate {
    func updateViewController(selectedRiding: String, province: String)
}

class Rest: NSObject {

    var request : NSMutableURLRequest
    var queryStr : String
    var delegate : RestDelegate?
    
    init(url: String, queryStr: String, httpMethod: String) {
        self.request = NSMutableURLRequest()
        self.request.HTTPMethod = httpMethod
        self.request.URL = NSURL(string: url)
        self.queryStr = queryStr
    }
    
    func getRiding(postcode: String)
    {
        var riding : String?
        var province: String?
        
        self.request.URL = NSURL(string: self.request.URL!.description + postcode + "?" + self.queryStr)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            do {
//                var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            
                let jsonResult: NSDictionary! = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
                if (jsonResult != nil){
                    // process jsonResult
                    riding = jsonResult?["boundaries_concordance"]?[0]?["name"] as? String
                    province = jsonResult?["province"] as? String
                    self.delegate!.updateViewController(riding!,province: province!)
                
                } else {
                // couldn't load JSON, look at error
                }
            } catch {
            
            }
            
            
        })
    
    }
    
    
}
