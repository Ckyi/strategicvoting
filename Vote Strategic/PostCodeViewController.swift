//
//  PostCodeViewController.swift
//  Vote Strategic
//
//  Created by orange on 2015-09-08.
//  Copyright (c) 2015 Vote Strategic. All rights reserved.
//

import UIKit

class PostCodeViewController: UIViewController, RestDelegate {

    @IBOutlet var postcodeTextfield: UITextField!
    @IBOutlet var proceedButton: UIButton!
    var rest: Rest!
    @IBOutlet var showSummaryButton: UIButton!
    @IBOutlet var ridingLabel: UILabel!
    @IBOutlet var ridingValue: UILabel!
    var provinceVal: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.proceedButton.layer.cornerRadius = 4;
        self.proceedButton.layer.borderWidth = 1;
        self.proceedButton.layer.borderColor = UIColor.blackColor().CGColor
        self.showSummaryButton.layer.cornerRadius = 4;
        self.showSummaryButton.layer.borderWidth = 1;
        self.showSummaryButton.layer.borderColor = UIColor.blackColor().CGColor
        
        self.rest = Rest(url: "http://represent.opennorth.ca/postcodes/", queryStr: "sets=federal-electoral-districts", httpMethod: "GET")
        self.rest.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func isValidPostcode(postcode: String)->Bool
    {
        let postcodeRegEx = "^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1} *\\d{1}[A-Za-z]{1}\\d{1}$"
        let postcodeTest = NSPredicate(format:"SELF MATCHES %@", postcodeRegEx)
        var test = postcodeTest.evaluateWithObject(postcode)
        return postcodeTest.evaluateWithObject(postcode)
    }
    
    private func transformValidPostCode(postcode: String)->String
    {
        var validString: String = ""
        
        validString = (postcode as NSString).stringByReplacingOccurrencesOfString(" ", withString: "")
        
        validString = validString.uppercaseString
        
        return validString
    }
    
    func updateViewController(selectedRiding: String, province: String) {
        dispatch_async(dispatch_get_main_queue(),{
            self.ridingLabel.hidden = false
            self.ridingValue.text = selectedRiding
            self.ridingValue.hidden = false
            self.showSummaryButton.hidden = false
            self.provinceVal = province
        })
    }
    
    @IBAction func proceedTouchUpInside(sender: UIButton) {
        //var restObj = Rest(url: "http://represent.opennorth.ca/postcodes/", queryStr: "sets=federal-electoral-districts", httpMethod: "GET", callerView: self)
        //"http://represent.opennorth.ca/postcodes/K2C4B2/?sets=federal-electoral-districts"

        if(isValidPostcode(postcodeTextfield.text!)){
            var transformedPostcode = self.transformValidPostCode(postcodeTextfield.text!)
            rest.getRiding(transformedPostcode)
        }
        else{
        //error handling here
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowResultView"
        {
            if let destinationVC = segue.destinationViewController as? ResultViewController{
               destinationVC.ridingData = self.ridingValue.text
               destinationVC.province = self.provinceVal
            }
            
        }

        
    }
    

}

